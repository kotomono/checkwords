import React, {Component} from 'react';
import {View, TextInput, TouchableOpacity, Text} from 'react-native';
import styles from './style';

/*
const sameCharsWords = (wordOne, wordTwo) => {
    let i = wordOne.length + 1;
    while (i--) {
      if (wordTwo.indexOf(wordOne[i]) >= 0) {
        wordTwo.splice(wordTwo.indexOf(wordOne[i]), 1);
      }
    }

    if (wordTwo.length) {
      return false
    }
    return true
}
*/

export default class App extends Component {
  state = {
    currentWord: '',
    result: '',
    words: [],
  };

  onChangeText = currentWord => {
    this.setState({currentWord});
  };

  sortAlphabetically = word => {
    return word
      .toLowerCase()
      .split('')
      .sort()
      .join('');
  };

  getAllIndexesOfOccurrences = (arr, value) => {
    const indexes = [];
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === value) {
        indexes.push(i);
      }
    }
    return indexes;
  };

  concatResult = () => {
    const {words} = this.state;
    let result = '';
    const alphabetWords = [];

    //fill new array with alphabetically ordered words
    for (const word of words) {
      alphabetWords.push(this.sortAlphabetically(word));
    }

    for (const word of alphabetWords) {
      if (word !== '') {
        //make an array of positions of all occurrences
        const indexesArray = this.getAllIndexesOfOccurrences(
          alphabetWords,
          word,
        );
        for (const index of indexesArray) {
          result += words[index] + ',';
        }

        //transform used words in empty string without removing item from array, so i keep indexes from original array of words
        for (const index of indexesArray) {
          if (alphabetWords[index] === word) {
            alphabetWords[index] = '';
          }
        }
        result += '\n';
      }
    }

    this.setState({result});
  };

  onPress = () => {
    const {currentWord, words} = this.state;
    words.push(currentWord);
    this.concatResult();
  };

  render() {
    const {result} = this.state;

    return (
      <View style={styles.container}>
        <TextInput style={styles.textInput} onChangeText={this.onChangeText} />
        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text style={styles.buttonTitle}>INVIO</Text>
        </TouchableOpacity>
        <View style={styles.resultContainer}>
          <Text style={styles.resultText}>{result}</Text>
        </View>
      </View>
    );
  }
}
